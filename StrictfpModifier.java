strictfp class StrictfpModifier{
	static double a= 0.1000;
	static double b=2.444;
	
	public static void main(String args[]){
		StrictfpModifier s=new StrictfpModifier();
		System.out.println(s.sum(a,b));
	}
	public double sum(double a,double b){
		return a+b;
	}
}
